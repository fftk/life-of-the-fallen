/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Movement;
import java.applet.Applet;

import java.awt.*;

import java.awt.event.*;

import com.sun.j3d.utils.applet.MainFrame;

import com.sun.j3d.utils.geometry.*;

import com.sun.j3d.utils.universe.*;

import javax.media.j3d.*;

import javax.vecmath.*;
/**
 *
 * @author Marie
 */
public class CamraMovement extends Applet implements KeyListener {
    Float x = -1.0f;
        Float z = 10.0f;
        Float y = 10.0f;

    Frame frame;
    SimpleUniverse simpleU; // this is the SimpleUniverse Class that is used for Java3D
     public void init() { 
        setLayout(new BorderLayout()); // standard Java code for BorderLayout
    	Canvas3D c = new Canvas3D(SimpleUniverse.getPreferredConfiguration()); 
    	add("Center", c);    
	simpleU= new SimpleUniverse(c); // setup the SimpleUniverse, attach the Canvas3D
        Vector3f te = new Vector3f(x, y, z);
    	BranchGroup scene = Camra(te); 
        scene.compile(); 


        simpleU.addBranchGraph(scene); //add your SceneGraph to the SimpleUniverse

    }
        
     @Override
    public void keyTyped(KeyEvent ke) {
        
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        while(ke.equals("W")){
             x++;
        }while(ke.equals("S")){
            x--;
        }while(ke.equals("D" + "W")){
             x++;
            y++;
        }while(ke.equals("A" + "W")){
            x--;
            y--;
        }while(ke.equals("A" + "S")){
            x--;
            y++;
        }while(ke.equals("D" + "S")){
            x--;
            y--;
        }if(ke.equals("1")){
            System.out.print(x + y + z);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
    }
    public BranchGroup Camra(Vector3f de){
        Transform3D cc3d = new Transform3D(); // a Transform3D allows a TransformGroup to move
	cc3d.setTranslation(de);	//Here we will create a basic SceneGraph with a ColorCube object

	// This BranchGroup is the root of the SceneGraph, 'objRoot' is the name I use,
	// and it is typically the standard name for it, but can be named anything.


   	BranchGroup objRoot = new BranchGroup(); 

	TransformGroup cctg = new TransformGroup(); 	// a TransformGroup for the ColorCube called cctg

	ColorCube c = new ColorCube(0.5f);	// create a ColorCube object
    	cctg.addChild(c); 			// add ColorCube to cctg

 // set translation to x=0.8, y=1.0, z= -2.0

	cctg.setTransform(cc3d); // set Transform for TransformGroup

	objRoot.addChild(cctg); // add cctg to Root

	ViewingPlatform vp = simpleU.getViewingPlatform(); // get the ViewingPlatform of the SimpleUniverse

	TransformGroup View_TransformGroup = vp.getMultiTransformGroup().getTransformGroup(0); // get the TransformGroup associated

	Transform3D View_Transform3D = new Transform3D();	// create a Transform3D for the ViewingPlatform
        View_TransformGroup.getTransform(View_Transform3D); // get the current 3d from the ViewingPlatform

	View_Transform3D.setTranslation(new Vector3f(x,y,z)); // set 3d to  x=0, y=-1, z= 5
	View_TransformGroup.setTransform(View_Transform3D);  // assign Transform3D to ViewPlatform

	// return Scene Graph
        return objRoot;
    }

    public static void main(String[] args){
        
        Frame frame = new MainFrame(new CamraMovement(), 500, 500);

    }
    public Frame fd(){
        return frame;
    }
   


}
