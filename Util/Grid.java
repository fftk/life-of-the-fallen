/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import javax.swing.JPanel; //imports JFrame library
import javax.swing.JButton; //imports JButton library
import java.awt.GridLayout; //imports GridLayout library
 
public class Grid {
 
        JPanel frame=new JPanel(); //creates frame
        JButton[][] grid; //names the grid of buttons
 
        public Grid(int width, int length){ //constructor
                frame.setLayout(new GridLayout(width,length)); //set layout
                grid=new JButton[width][length]; //allocate the size of grid
                frame.removeAll();
                for(int y=1; y<=length; y++){
                        for(int x=1; x<=width; x++){
                                grid[x][y]=new JButton("("+x+","+y+")"); //creates new button    
                                frame.add(grid[x][y]); //adds button to grid
                        }
                }

                frame.setVisible(true); //makes frame visible
        }

}