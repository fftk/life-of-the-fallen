/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;
import java.awt.MouseInfo;  
import java.awt.Point;

/**
 *
 * @author Marie
 */
public class MouseFinder {
    Point mouseXY = null;
    public MouseFinder(){
       mouseXY = new Point(MouseInfo.getPointerInfo().getLocation().y, MouseInfo.getPointerInfo().getLocation().x);  
    }
    public Point xy(){
        return mouseXY;
    }
}
