/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ModCreator;

import java.awt.GridLayout;
import javax.swing.*;
import javax.swing.JPanel;

/**
 *
 * @author Marie
 */
public class JTextFieldGridutil {

 
        JPanel frame=new JPanel(); //creates frame
        JTextField[][] grid; //names the grid of buttons
 
        public JTextFieldGridutil(int width, int length){ //constructor
                frame.setLayout(new GridLayout(width,length)); //set layout
                grid=new JTextField[width][length]; //allocate the size of grid
                
                for(int y=0; y<length; y++){
                        for(int x=0; x<width; x++){
                                grid[x][y]=new JTextField("("+x+","+y+")"); //creates new button    
                                frame.add(grid[x][y]); //adds button to grid
                        }
                }

                frame.setVisible(true); //makes frame visible
        }
        public JPanel pan(){
            return frame;
        }

}

