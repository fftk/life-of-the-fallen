/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ModCreator.Worldmaker;

import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.universe.SimpleUniverse;
import java.awt.Color;
import javax.media.j3d.BoundingBox;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TransformGroup;
import javax.swing.JOptionPane;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

/**
 *
 * @author Marie
 */
public class LandLoader extends SimpleUniverse{



    
    public LandLoader() {
        BranchGroup scene = new BranchGroup();
        
       
     int primflags = Primitive.GENERATE_NORMALS
        + Primitive.GENERATE_TEXTURE_COORDS;
        ObjectFile loader = new ObjectFile(ObjectFile.RESIZE);
        
        Scene modelScene = null;


        Shape3D model = new Shape3D();
 
        
        try{
            modelScene = loader.load("C:\\Users\\Marie\\Desktop\\TestLandscape.obj");

               
              
            //JOptionPane.showMessageDialog(null, model.getAppearance().getMaterial());
            model = (Shape3D) modelScene.getSceneGroup().getChild(0);

            model.setAppearance(null);

            model.setCapability(TransformGroup.ALLOW_COLLISION_BOUNDS_READ);
            model.setCapability(TransformGroup.ALLOW_COLLISION_BOUNDS_WRITE);
            modelScene.getSceneGroup().removeChild(0); 
             model.setCapability(TransformGroup.ENABLE_COLLISION_REPORTING);

            BoundingBox box = new BoundingBox();
           box.equals(model.cloneNode(true));
           model.setCollisionBounds(box);
           
           
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e.toString());
        }
        DirectionalLight lighting = new DirectionalLight(new Color3f(Color.WHITE), new Vector3f(0f, 0f, -1f));
        lighting.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 1.0), 100));
        //scene.addChild(modelScene.getSceneGroup());
  
          scene.addChild(model);  

          
          
        scene.addChild(lighting);
        this.addBranchGraph(scene);
        this.getViewingPlatform().setNominalViewingTransform();
    }

}
