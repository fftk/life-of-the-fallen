Life-of-the-Fallen
==================

Code for a Game/Game engine in development

The Shell of the game including modding resources. 

Concept
======

The Basic concept of the game is a fully Customization apocalypse Sim where the player has to rebuild society and/or 
survive for as long as he/she can.


Difficulty
==========

The games difficulty is where the player starts on a timeline.

Hardest- Before Apocalypse

Hard- At the Beginning of the  Apocalypse

Normal- Post  Apocalypse

Easy- A few Years before the restart of society

Realism Level
=============

The game allows the player to chose the level of realism, which is like difficulty however it is subgroups, thus

Surreal - Health is measured in both Hp and infection Chance(see Life)

Real - Health is measured in infection chance

Game - Health is in HP

Life
====

The Game, depending on the Realism Level, measures life in Infection Chance. IE, if you break a leg, your 
Infection Chance(IC) goes up. The game will then make a random number at the time of the injury, and in 15 min periods
and check if it is higher than or lower than your IC. If it matches you get a Infection, 
and thus have a set time to live and treat the Infection or die.
There are other things that will happen as well. The location of where you have the infection will count as well,
say you get shot in the head and manage to survive, you will be unconscious for a long period of time. When you wake up, 
You will have a Blurry screen and will stumble a lot. 

Locations
=========

There will be 3 major Locations in the game with disadvantages and advantages.

City - The Most items to loot(See Looting) However IC goes up

Suburbs - Some items to loot However More feral Animals IE dogs

Rural - Lowers Ic, More Resources(See Resources) However Less pre-made Loot

Crafting
========

The Game will have crafting systems in placed, where the user is required to find the right Tool for the Job, Ie Car
Parts to make a car needs a Car carting table wheres using the car parts for its parts needs a assembly table. There 
you can combine like parts to make your own items IE a silencer on a gun with a custom barrel and Stock.


Loot
====

There will be multiple, Items in the game where every one of them can be made, from either higher end pieces or elemental
pieces(see Resources) Some will be consumable, others tools, and some are just pre-made pieces for a larger object. 

Resources
==========

The Resources in the game are elemental pieces, ie fundamental stages of an item, such as wood, or iron or clay.
Most can be found in rural areas(see Locations) however the resources will be in every area.

Apocalypses
============
The Game will allow the player to chose his apocalypse in the begging, and in the hardest game mode(see Difficulty)
he may opt to be part of orchestrating it. 

Main Apocalypses(Subjected to change)

-Zombie
-Alien
-Nuclear
-Biological
-Solar
-Space Station(See Special Apocalypses)
-Food Chain race(another being evolving to be better than us)
-War zone(See Special Apocalypses
-Meteorite
-EMP
-Invasion(See Special Apocalypses)

Special Apocalypses
====================
A few Special Apocalypses are going to be planed for future versions where the user has a goal other than surviving
and rebuilding.

interaction
===========
The World will be divided into zone segments, where a certain area will be affected by the player being with in, IE a 
bubble around a player will allow time to flow. This will allow events(See Events) to happen and cause buildings to 
degrade every time the exit the bubble(see Time). Through out the apocalypses you start out as a survivor and have 
to earn trust in strongholds(See Strongholds) to become there leader. once you have enough you can form a society, and 
then go on to rebuild the land into its former glory. 

Events
=====
As Time goes on,(See Time) Events in Strongholds(See Strongholds) will happen randomly, These could be good or bad, And 
they are selected randomly. 
Example Events

Zombies break through defenses-

Aliens Locate Stronghold-

Unknown Nuclear ordinance goes off-

babie is born-

Surplus of food-

Famine-

Time
====
the effects of time will be measured by a bubble around the player as he/she progresses through the game. The bubble will
correct its position every 4 min. so it is not dragging the game down in fps. Time affects both building structure and 
Events. The Buildings will eventually degrade to the point where it collapses and falls down possibly injuring the player, 
whether or not he is inside it our outside it.

Strongholds
===========

A Stronghold is a location where a group of survivors have banded together to form small committees, which have ranging 
political and economic systems. The goal of the game is to gather enough Strongholds together to form a society. The 
Strongholds will however fight each other over resources and land.

Winning
=======

There is no winning this game, there is only surviving as long as possible. There is however goals to the game in which 
give the player a boost up in the world. When a stronghold is formed you have the ability to govern it. while you do this
you run the risk of three Main things

-Assassination
-Governmental Upheaval and assassination
-Infestation
-(Along with the negative Events(See Events))

Life
====

As you go along in your life, a Goal is to never die, your character will age, he will get injured and he will eventually 
die, if your character dies you lose the game unless you accomplish 2 things

-Finding a women/man that is willing to marry you
-Having a child

If these 2 things are accomplished when you die your character will swap to your child when he is a adult. this will erase
your stats but you keep your crafting book and your items, thought they will take some damage.

Scavenging
==========

As a player you must scavenge for food water and other supplies to survive, This can be completely striping a car for
electronics, metal, batters, and gas, or it can repairing a car so it runs. When you Scavenge you will make noise that 
will scare off, then attract animals, and attract predictors/Humans. There will be no limit to what you can carry, to a cretin
degree, however you stamina and speed will take a hit until adrenaline kicks in(See Adrenaline)

Bases
=====

Before you find a society, or buy a hovel in a stronghold, you must have a camp. You can use this camp to store objects or
cars, thought they will run the risk of getting stolen. You can find electronics to build security systems as well as
making/finding generators to power them.

Adrenaline
==========

At a certain threshold depending on the player(See Player) of IC, Adrenaline will activate negating all wound effects(See Wounds)
and boosting stamina and speed as well as slowing down the players environment but not the player.

Wounds
======

A Player, upon receiving a wound will have a negating effect on his stats, ie you break your leg, you cant carry anything
or run. When a serious injury occurs and there is a enemy within the players eyesight, adrenaline will kick in until 
the player stops moving, or the enemy has not been seen for 2 min. As well as the player will gain a possible minor diseases that could progress to majorif IC is over 50%, these diseases include but not all, Gangrine, Amonia, footfungus, cold, Diarea, vomiting, flu, and heavy sweating. each one add in a special factor/stat to the player making him/her more vulnerbal to a certin aspect.


Specal diseases
================

There will be Specal diseases\mental impares that will effect your gameplay, for example, your carater over time may become paranoid, or insane. these effects will unlock ghost like flicker of movements and cause your caracter to react to them. IE your exploring a Dark office building with overturned desk when you start hearing say laughter, then as you go down a hall you see a dark shape run across the end wall, or go up stairs thinking your going down, see a survivor when its a zombie. After a certin time of paranoia inside a building your character will start to see things IE a door where there is a window. and thus cause you to not trust your sences and may possibly lead to your death.

Death
=====
Upon a players death, the game will create a new character that starts at the same time as the other however, when the time
in game is the same as when the players death, his bag/base will appear with all his items, but the new player will be
regarded as an enemy to the others security systems at his base.

