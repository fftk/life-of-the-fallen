/*
 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;
import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;
import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

public class WorldLoader {

       static TextureLoader loader = new TextureLoader("C:/Users/Marie/Desktop/Paint Layer 1.jpg",
        "RGP", new Container());
    static Texture texture = loader.getTexture();
    
   public WorldLoader() {
        SimpleUniverse universe = new SimpleUniverse();
        BranchGroup scene = new BranchGroup();
        
        texture.setBoundaryModeS(Texture.WRAP);
    texture.setBoundaryModeT(Texture.WRAP);
    texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.0f));
    TextureAttributes texAttr = new TextureAttributes();
    texAttr.setTextureMode(TextureAttributes.MODULATE);
    Appearance ap = new Appearance();
    ap.setTexture(texture);
    ap.setTextureAttributes(texAttr);
     int primflags = Primitive.GENERATE_NORMALS
        + Primitive.GENERATE_TEXTURE_COORDS;
        ObjectFile loader = new ObjectFile(ObjectFile.RESIZE);
        
        Scene modelScene = null;
        Shape3D model = new Shape3D();
        try{
            modelScene = loader.load("C:/Users/Marie/Desktop/Test1.obj");
            //JOptionPane.showMessageDialog(null, model.getAppearance().getMaterial());
            model = (Shape3D) modelScene.getSceneGroup().getChild(0);
            model.setAppearance(ap);
           modelScene.getSceneGroup().removeChild(0);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e.toString());
        }
        BranchGroup objRoot = new BranchGroup(); 

	TransformGroup cctg = new TransformGroup(); 
        cctg.addChild(model);
        // a TransformGroup for the ColorCube called cctg
        Transform3D cc3d = new Transform3D(); // a Transform3D allows a TransformGroup to move
	cc3d.setTranslation(new Vector3f (0.8f ,1.0f ,-2.0f )); // set translation to x=0.8, y=1.0, z= -2.0

	cctg.setTransform(cc3d); // set Transform for TransformGroup

	objRoot.addChild(cctg); // add cctg to Root

	ViewingPlatform vp = universe.getViewingPlatform(); // get the ViewingPlatform of the SimpleUniverse

	TransformGroup View_TransformGroup = vp.getMultiTransformGroup().getTransformGroup(0); // get the TransformGroup associated

	Transform3D View_Transform3D = new Transform3D();	// create a Transform3D for the ViewingPlatform
        View_TransformGroup.getTransform(View_Transform3D); // get the current 3d from the ViewingPlatform

	View_Transform3D.setTranslation(new Vector3f(0.0f,-1.0f,5.0f)); // set 3d to  x=0, y=-1, z= 5
	View_TransformGroup.setTransform(View_Transform3D);  // assign Transform3D to ViewPlatform

        DirectionalLight lighting = new DirectionalLight(new Color3f(Color.WHITE), new Vector3f(0f, 0f, -1f));
        lighting.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 1.0), 100));
        //scene.addChild(modelScene.getSceneGroup());
        scene.addChild(lighting);
        universe.addBranchGraph(scene);
        universe.addBranchGraph(objRoot);
    }
   public static void main(String[] args){
       new WorldLoader();
   }
}
